import 'package:flutter/material.dart';

import 'pages/home_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'APP DE MUESTRA CONSUMO DE API',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}