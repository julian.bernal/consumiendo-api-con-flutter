import 'dart:developer';
import 'package:flutter/material.dart';

import '../models/misc_models.dart' as model;
import '../services/services.dart';
import 'full_screen_image.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {

  List<model.Characters> characters = [];

  @override
  void initState() {
    super.initState();
    _paintCharacters();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black12,
      appBar: AppBar(
        title: const Text('API Rick and morty'),
        centerTitle: true,
        toolbarHeight: 45.0,
        backgroundColor: const Color(0xFF162947),
        actions: [
          IconButton(
            tooltip: 'Reload Characters',
            onPressed: () => _paintCharacters(), 
            icon: const Icon(Icons.replay_outlined)
          )
        ],
      ),
      body: characters.isNotEmpty ? ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: characters.length,
        itemBuilder: (context, index) {
          final item = characters[index];
          return Container(
            padding: EdgeInsets.only(
              top: size.height * 0.2,
              left: 5.0,
              right: 5.0
            ),
            child: Column(
              children: [
                InkWell(
                  child: Image.network(item.image),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (BuildContext context) => 
                        FullScreenImage(
                          name: item.name, urlImg: item.image
                        )
                      )
                    );
                  },
                ),
                _text(text :item.name),
                _text(text :item.species),
              ],
            ),
          );
        },
      ): const Text('sin Data'),
    );
  }

  void _paintCharacters(){
    Services().getCharacters().then((data){
      if (data['response'] == 200) {
        characters.clear();
        setState(() {
          characters = model.Model.fromJson(data['data2']).results;
        });
        inspect(characters);
        return;
      }
      print('error de API');
    });
  }

  Text _text({required String text}){
    return Text(text, 
      style: const TextStyle(
        color:  Color(0xffbdff9d),
        fontSize: 21
      )
    );
  }

}