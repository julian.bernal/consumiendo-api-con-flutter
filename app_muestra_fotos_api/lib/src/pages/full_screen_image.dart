import 'package:flutter/material.dart';

class FullScreenImage extends StatelessWidget {
  final String urlImg;
  final String name;
  const FullScreenImage({Key? key, required this.urlImg, required this.name}) : 
  super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        title: Text(name, style: const TextStyle(
          color:  Color(0xffbdff9d),
          fontSize: 21)
        ),
        elevation: 0.0,
        toolbarHeight: 45.0,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_rounded,
            color: Color(0xffbdff9d),
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: InteractiveViewer(
        child: Center(
          child: Hero(
            tag: urlImg,
            child: Image.network(
              urlImg,
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
      ),
    );
  }
}