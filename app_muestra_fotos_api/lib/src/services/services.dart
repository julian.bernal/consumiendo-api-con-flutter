import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;

import '../constants/constants.dart';

class Services {
  
  /// Servicio para obtener personajes aleatorio de la serie Rick and Morty
  Future<Map<String, dynamic>> getCharacters() async {

    try {

      String page = Random().nextInt(43).toString();

      final request = await http.get(
        Uri.parse('${Constants.baseUrlApi}character?page=$page')
      ); 

      if (request.statusCode == 200) {
        Map<String, dynamic> data = json.decode(request.body); 
        return {
          'response': 200, 
          'data'    : data['results'],
          'data2'   : data
        };  
      }

      return {
        'response': 500, 
        'data'    : null, 
        'mesagge' : 'Error server ${request.body}'
      };
      
    } catch (e) {
      return {'response' : 0, 'mesagge' : e.toString()};
    }
    
  }

}